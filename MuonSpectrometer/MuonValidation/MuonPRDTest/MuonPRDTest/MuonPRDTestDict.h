/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONPRDTEST_MUONPRDTESTDICT_H
#define MUONPRDTEST_MUONPRDTESTDICT_H
#include <MuonPRDTest/MDTDigitVariables.h>
#include <MuonPRDTest/MDTSDOVariables.h>
#include <MuonPRDTest/MDTSimHitVariables.h>
#include <MuonPRDTest/RPCDigitVariables.h>
#include <MuonPRDTest/RPCSDOVariables.h>
#include <MuonPRDTest/RPCSimHitVariables.h>
#include <MuonPRDTest/CSCDigitVariables.h>
#include <MuonPRDTest/CSCSDOVariables.h>
#include <MuonPRDTest/CSCSimHitVariables.h>
#include <MuonPRDTest/TGCDigitVariables.h>
#include <MuonPRDTest/TGCSDOVariables.h>
#include <MuonPRDTest/TGCSimHitVariables.h>
#include <MuonPRDTest/TruthVariables.h>
#endif
